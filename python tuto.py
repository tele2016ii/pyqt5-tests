print(10/2)   # DIVISION
print(10//2)  # DIVISION ENTERA
print(10%2)   # MODULO DIVISION
print(2**2)   # POTENCIA
print(17/3, round(17/3))  # ROUND

# FLOAT TYPE
print(float(1.25), float(5))
print(float(1e3))
print(float(1e-3))

# STRINGS
str = "Oregano"
print(str, len(str))

# LISTS
print("## LIST ##")
lst = [1, 2, 3, 4, 5]
print(lst, len(lst))
lst += [6, 7, 8]
print(lst)
lst[4] = 55
print(lst)
lst[0:3] = [11, 22, 33]
print(lst)
lst[1:3] = []
print(lst)
print([['a', 'b', 'c'], [1, 2, 3]])

# IF
from random import randint
print("## IF ##")
x = randint(-1, 2)
if x < 0:
    x = 0
    print(x, 'negative changed to zero ')
elif x == 0:
    print(x, 'zero')
elif x == 1:
    print(x, 'single')
else:
    print(x, 'more')

# FOR
print("## FOR ##")
words = ["casa", "carro", "pelota"]
for w in words:
    print(w, len(w), end=", ")
print()

for i, w in enumerate(words):
    print(i, w, end=", ")
print()

for i in range(len(words)):
    print(i, words[i], end=", ")
print()

for i in range(5):
    print(i, end=", ")
print()

for i in range(5, 10):
    print(i, end=", ")
print()

for i in range(0, 10, 3):
    print(i, end=", ")
print()

for i in range(-10, -100, -30):
    print(i, end=", ")
print()


# FUNCION AND WHILE
print("## FUNCION AND WHILE ##")
def fib(n): # return Fibonacci series up to n
    """
    Return a list containing the Fibonacci series up to n.
    :param n:
    :return:
    """
    r = []
    a, b = 0, 1  # DOBLE ASIGNACION
    while b < n:
        r.append(a)
        a, b = b, a+b
    return r

print(fib(2000))

# PARAMETROS FUNCIONES
def test(a, b="texto", c= 100):
    print(a, b, c)

test("queso")
test("queso", "texto cambiado")
test("queso", "texto cambiado2", 1000)
test("3D", c=0)

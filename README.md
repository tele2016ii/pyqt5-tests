# PyQt5 Tests
-----
### Pruebas en PyQt5 de graficas y calculos de modulacion PM, haciendo uso de Numpy, Matplotlib, PyQtChart.

### Desarrollo
Desde consola, teniendo instalado Python 3.6 con pip:

- Dependencias Globales:
```sh
$ python -m pip install --upgrade pip setuptools wheel     # ACTUALIZACION DE PIP Y OTROS
$ pip install virtualenv                                   # INSTALACION DE ENTORNO VIRTUAL
```

- Crear/Activar/desactivar Virtual Enviroment:
```sh
$ virtualenv env                      # CREAR ENTORNO VIRTUAL
$ virtualenv -p /path/to/python/ env  # CREAR ENTORNO VIRTUAL ESPECIFICANDO VERSION DE PYTHON
$ env/Scripts/activate                # ACTIVAR ENTORNO VIRTUAL
$ env/bin/deactivate                  # DESACTIVAR ENTORNO VIRTUAL
```

- Dependencias de Proyecto:
> Teniendo el entorno virtual activado
```sh
(env)$ pip install -r requirements.txt  # INSTALAR DEPENDENCIAS DE PROYECTO EN ENTORNO VIRTUAL ACTIVADO
```
> En caso de fallos instalar paquetes manualmente
```sh
(env)$ pip install numpy scipy matplotlib  
(env)$ pip3 install PyQt5 PyQtChart        
```
> En caso de errores a la hora de instalar `scipy` en windows, descargar los paquetes:
> `numpy‑1.12.0+mkl‑cp36‑cp36m‑win32.whl` y `scipy‑0.18.1‑cp36‑cp36m‑win32.whl` desde 
> [Windows Binaries for Python Extension Packages](http://www.lfd.uci.edu/~gohlke/pythonlibs/)
```sh
(env)$ pip install numpy‑1.12.0+mkl‑cp36‑cp36m‑win32.whl
(env)$ pip install scipy‑0.18.1‑cp36‑cp36m‑win32.whl     
```
### Running Scripts
```sh
(env)$ python main.py
```


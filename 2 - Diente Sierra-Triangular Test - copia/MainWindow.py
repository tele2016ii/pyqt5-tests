import numpy as np
import re
from PyQt5.QtWidgets import QMainWindow, QWidget, QLabel, QPushButton, QVBoxLayout, \
    QHBoxLayout, QLineEdit, QMessageBox, QComboBox
from matplotlib.pyplot import figure, show
from numpy import arange, sin, cos, pi, arctan, tan
from scipy.signal import sawtooth

SENOIDALES    = ["Seno", "Coseno"]
NO_SENOIDALES = ["Diente de Sierra", "Triangular"]
regex_Sen_Cos = '^([0-9]*)(cos|sin)\(([0-9]*)?(pi)?([0-9]*)?t\)$'


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.pb_Modular = QPushButton("Modular")

        # LAYOUT CARRIER
        self.l_Portadora = QLabel("Portadora Diente Sierra")
        self.cb_funcionc = QComboBox()
        self.cb_funcionc.addItems(NO_SENOIDALES)
        self.le_vc = QLineEdit("5")
        self.le_vc.setPlaceholderText("Introduce Amplitud")
        self.le_fc = QLineEdit("250")
        self.le_fc.setPlaceholderText("Introduce Frecuencia")
        self.CLayout = QVBoxLayout()
        self.CLayout.addWidget(self.l_Portadora)
        self.CLayout.addWidget(self.cb_funcionc)
        self.CLayout.addWidget(self.le_vc)
        self.CLayout.addWidget(self.le_fc)
        # LAYOUT CARRIER

        # LAYOUT MODULADORA
        self.l_Moduladora = QLabel("Moduladora")
        self.funcM = QComboBox()
        self.funcM.addItems(SENOIDALES)
        self.le_vmt = QLineEdit("10sin(2pi100t)")
        self.le_vmt.setPlaceholderText("Intruce Modelo")
        self.MLayout = QVBoxLayout()
        self.MLayout.addWidget(self.l_Moduladora)
        self.MLayout.addWidget(self.funcM)
        self.MLayout.addWidget(self.le_vmt)
        # LAYOUT MODULADORA

        # MAIN LAYOUT
        self.mainLayout = QHBoxLayout()
        self.mainLayout.addLayout(self.CLayout)
        self.mainLayout.addLayout(self.MLayout)

        self.bmLayout = QHBoxLayout()
        self.bmLayout.addStretch()
        self.bmLayout.addWidget(self.pb_Modular)
        self.bmLayout.addStretch()

        self.container = QVBoxLayout()
        self.container.addLayout(self.mainLayout)
        self.container.addLayout(self.bmLayout)

        self.centralWidget = QWidget()
        self.centralWidget.setLayout(self.container)
        self.setCentralWidget(self.centralWidget)

        self.pb_Modular.clicked.connect(self.modular)

        self.setWindowTitle('Prueba 1')

    def modular(self):
        patron = re.compile(regex_Sen_Cos)

        # PORTADORA
        vc    = float(self.le_vc.text())
        fc    = float(self.le_fc.text())
        tipoc = 1 if self.cb_funcionc.currentIndex() == 0 else 0.5

        # MODULADORA
        tupla_vmt = patron.findall(self.le_vmt.text())[0]
        vm = float(tupla_vmt[0])
        fm = float(tupla_vmt[4])

        # Datos Generales
        K = 20
        fig = figure(1)

        # GRAFICA PORTADORA
        Tc = 1/fc
        t1 = np.linspace(0.0, 25*Tc, 10**5)
        wc = 2 * pi * fc
        vct = vc * sawtooth(wc * t1, tipoc)
        ax1 = fig.add_subplot(221)
        ax1.plot(t1, vct)
        ax1.grid(True)
        ax1.set_title('Señal Portadora')
        ax1.set_ylim(-vc-1, vc+1)
        # ax1.axis([0, 10*Tc, -vc-1, vc+1])
        ax1.set_ylabel('Voltios')
        ax1.set_xlabel('Tiempo')
        # GRAFICA PORTADORA

        # GRAFICA MODULADORA
        Tf = 1/fm
        t2 = np.linspace(0.0, 10*Tf, 10**5)
        wm = 2 * pi * fm
        vmt = vm * sin(wm * t2)
        ax2 = fig.add_subplot(222)
        ax2.plot(t2, vmt)
        ax2.grid(True)
        ax2.set_title('Señal Moduladora')
        ax2.set_ylim(-vm-1, vm+1)
        ax2.set_ylabel('Voltios')
        ax2.set_xlabel('Tiempo')
        # GRAFICA MODULADORA

        # GRAFICA MODULADA DE FASE
        # Tpm = 1/fpm
        t3 = np.linspace(0.0, 1, 10**5)
        ax3 = fig.add_subplot(2, 2, (3, 4))
        pmt = vc * sin(wc*t3 + K * vm * sin(wm * t3))
        ax3.plot(t3, pmt)
        ax3.grid(True)
        ax3.set_ylim(-100, 100)
        ax3.set_title('PM')
        ax3.set_ylabel('Voltios')
        ax3.set_xlabel('Tiempo')
        # GRAFICA MODULADA DE FASE

        show()

        # Evento para cuando la ventana se cierra
        # def closeEvent(self, event):
        #     resultado = QMessageBox.question(self, "Salir ...", "¿Seguro que quieres salir de la aplicación?",
        #                                      QMessageBox.Yes | QMessageBox.No)
        #     if resultado == QMessageBox.Yes:
        #         event.accept()
        #     else:
        #         event.ignore()

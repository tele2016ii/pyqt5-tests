import numpy as np
import re
from PyQt5.QtWidgets import QMainWindow, QWidget, QLabel, QPushButton, QVBoxLayout, \
    QHBoxLayout, QLineEdit, QMessageBox, QComboBox
from PyQt5.uic import loadUi
from matplotlib.pyplot import figure, show
from numpy import arange, sin, pi
funciones = ["Seno", "Coseno", "Diente de Sierra", "Triangular"]
regex_Sen_Cos = '^([0-9]*)(cos|sin)\(([0-9]*)?(pi)?([0-9]*)?t\)$'

class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        loadUi("mainwindow.ui", self)
        self.initUI()

    def initUI(self):
        self.pb_Modular.setText("chicharron")
        self.pb_Modular.setStyleSheet("background-color: #bada55; color: #333; border-radius: 5px; border: none; padding: 15px 30px; font-size: 16px;")
    #     self.pb_Modular = QPushButton("Modular")
    #
    #     # LAYOUT CARRIER
    #     self.funcC = QComboBox()
    #     self.funcC.addItems(funciones)
    #     self.vc = QLineEdit("20sin(2pi50t)")
    #     self.CLayout = QVBoxLayout()
    #     self.CLayout.addWidget(self.funcC)
    #     self.CLayout.addWidget(self.vc)
    #
    #     # LAYOUT MODULADORA
    #     self.funcM = QComboBox()
    #     self.funcM.addItems(funciones)
    #     self.vm = QLineEdit("13sin(2pi10t)")
    #     self.MLayout = QVBoxLayout()
    #     self.MLayout.addWidget(self.funcM)
    #     self.MLayout.addWidget(self.vm)
    #
    #     # MAIN LAYOUT
    #     self.mainLayout = QHBoxLayout()
    #     self.mainLayout.addLayout(self.CLayout)
    #     self.mainLayout.addLayout(self.MLayout)
    #
    #     self.bmLayout = QHBoxLayout()
    #     self.bmLayout.addStretch()
    #     self.bmLayout.addWidget(self.pb_Modular)
    #     self.bmLayout.addStretch()
    #
    #     self.container = QVBoxLayout()
    #     self.container.addLayout(self.mainLayout)
    #     self.container.addLayout(self.bmLayout)
    #
    #     self.centralWidget = QWidget()
    #     self.centralWidget.setLayout(self.container)
    #     self.setCentralWidget(self.centralWidget)
    #
    #     self.pb_Modular.clicked.connect(self.modular)
    #
    #     self.setWindowTitle('Prueba 1')
    #
    # def modular(self):
    #     # Portadora
    #     patron = re.compile(regex_Sen_Cos)
    #     self.tc = patron.findall(self.vc.text())[0]
    #     self.Ec = float(self.tc[0])
    #     self.fc = float(self.tc[4])
    #
    #     # Moduladora
    #     self.tm = patron.findall(self.vm.text())[0]
    #     self.Em = float(self.tm[0])
    #     self.fm = float(self.tm[4])
    #
    #     # Datos Generales
    #     self.m = self.Em/self.Ec
    #
    #     t = arange(0.0, 1, 10**(-5.7))
    #
    #     fig = figure(1)
    #
    #     ax1 = fig.add_subplot(221)
    #     ax1.plot(t, self.Ec * sin(2 * pi * self.fc * t))
    #     ax1.grid(True)
    #     ax1.set_ylim(-self.Ec-10, self.Ec+10)
    #     ax1.set_ylabel('Voltios')
    #     ax1.set_title('Señal Portadora')
    #
    #     for label in ax1.get_xticklabels():
    #         label.set_color('r')
    #
    #     ax2 = fig.add_subplot(222)
    #     ax2.plot(t, self.Em * sin(2 * pi * self.fm * t))
    #     ax2.grid(True)
    #     ax2.set_ylim(-self.Em-10, self.Em+10)
    #     ax2.set_title('Señal Moduladora')
    #     ax2.set_ylabel('Voltios')
    #
    #     ax3 = fig.add_subplot(2, 2, (3, 4))
    #     ax3.plot(t, (1 + self.m * sin(2 * pi * self.fm * t))*(self.Ec * sin(2 * pi * self.fc * t)))
    #     ax3.grid(True)
    #     ax3.set_ylim(-50, 50)
    #     ax3.set_title('Señal Vam(t)')
    #     ax3.set_ylabel('Voltios')
    #
    #     show()

        # Evento para cuando la ventana se cierra
        # def closeEvent(self, event):
        #     resultado = QMessageBox.question(self, "Salir ...", "¿Seguro que quieres salir de la aplicación?",
        #                                      QMessageBox.Yes | QMessageBox.No)
        #     if resultado == QMessageBox.Yes:
        #         event.accept()
        #     else:
        #         event.ignore()
